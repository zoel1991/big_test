<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
	public function index()
	{
		$this->load->model('menu_model', '', TRUE);
		$menu = $this->menu_model->getData();

		foreach ($menu as $k => $v) {
			$raw_menu[$k]['id'] = $v->id;
			$raw_menu[$k]['nama'] = $v->nama;
			$child = $this->menu_model->countChild($v->id);
			if ($child > 0) {
				$child = $this->menu_model->getChild($v->id);
				foreach ($child as $k2 => $v2) {
					$raw_menu[$k]['child'][$k2]['id'] = $v2->id;
					$raw_menu[$k]['child'][$k2]['nama'] = $v2->nama;
				}
			}
		}
		$data['menu'] = $raw_menu;
		$this->load->view('menu/index', $data);
	}

	public function submit()
	{
		$this->load->model('user', '', TRUE);

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$password = md5($password);

		$submit['username'] = $username;
		$submit['password'] = $password;

		$this->user->post($submit);

		return '/index.php/Login';
	}
}
