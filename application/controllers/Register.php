<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends CI_Controller
{
	public function index()
	{
		$this->load->view('register/index');
	}

    public function submit()
	{
		$this->load->model('user','', TRUE);

		$username = $this->input->post('username');
        $password = $this->input->post('password');
        $password = md5($password);

		$submit['username'] = $username;
		$submit['password'] = $password;

		$this->user->post($submit);
        
		return '/index.php/Login';
	}
}
