<?php
class Menu_model extends CI_Model
{
    public function getData()
    {
        $query = $this->db->from('menu')->where('parent_id', NULL)->get();
        return $query->result();
    }

    public function getChild($parent_id)
    {
        $query = $this->db->query('select * from menu where parent_id = "' . $parent_id . '"');
        return $query->result();
    }

    public function countChild($parent_id)
    {
        $query = $this->db->query('select * from menu where parent_id = "' . $parent_id . '"');
        return count($query->result());
    }

    public function post($array)
    {
        $this->db->insert('menu', $array);
    }

    public function update_entry($id, $array)
    {
        $this->db->update('menu', $array, array('id' => $id));
    }
}
