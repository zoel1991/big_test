<?php
class User extends CI_Model
{
    public function getData()
    {
        $query = $this->db->get('user');
        return $query->result();
    }

    public function post($array)
    {
        $this->db->insert('user', $array);
    }

    public function update_entry($id, $array)
    {
        $this->db->update('user', $array, array('id' => $id));
    }
}
